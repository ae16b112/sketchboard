import express from 'express'
import http from 'http'
import socketio from "socket.io";
import {userJoin, getCurrentUser, getRoomUsers } from './usecase/users.js'
import mongoose from 'mongoose'
import cors from 'cors';
import jwt from "jwt-then";
import {router as userRouter} from './routes/user.js'
import {router as userRoomRouter} from './routes/userRoom.js'
import {router as imageRouter} from './routes/image.js'

import bodyParser from 'body-parser';

// init
const app = express();
app.use(express.json());
app.use(cors());
app.use("/user", userRouter )
app.use("/room", userRoomRouter )
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.set("view engine", "ejs");
app.use("/image", imageRouter )




mongoose.connect("mongodb://localhost:27017/app_users", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.on("error", (err) => {
  console.log("Mongoose Connection ERROR: " + err.message);
});

mongoose.connection.once("open", () => {
  console.log("MongoDB Connected!");
});

const server = app.listen(5000, () => {
  console.log("Server listening on port 5000");
});

const io = socketio(server, {
  allowEIO3: true,
  cors: {
    origin: true,
    methods: ['GET', 'POST'],
    credentials: true
  }
});

io.use(async (socket, next) => {
  try {
    const token = socket.handshake.query.token;
    const payload = await jwt.verify(token, "secret");
    socket.id = payload.id;
    next();
  } catch (err) {}
});

io.on('connection', (socket)=> {
  socket.on('joinRoom', ({ username, room }) => {
    const user = userJoin(socket.id, username, room);

    socket.join(user.room);

    // Broadcast when a user connects
    socket.broadcast
      .to(user.room)
      .emit(
        'message',
        `${user.username} has joined the board`
      );

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUsers(user.room)
    });
  });

  socket.on('annotation-data', (data)=> {
    const user = getCurrentUser(socket.id);
    io.to(user.room).emit('annotation-data', data);
  });

  socket.on('disconnect', () => {
    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUsers(user.room)
    });
 
  });

})


import express from "express";
import { catchErrors } from "../handlers/errorHandlers.js";
import {login, register} from "../controllers/userController.js";
export const router = express.Router()

router.post("/login", catchErrors(login));
router.post("/register", catchErrors(register));

//module.exports = router



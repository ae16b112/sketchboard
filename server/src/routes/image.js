import express from "express";
import { catchErrors } from "../handlers/errorHandlers.js";
import {saveImage, getImage} from "../controllers/imageController.js";
export const router = express.Router()
import multer from 'multer';

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
  
let upload = multer({ storage: storage });

router.post("/saveImage",upload.single('image'), catchErrors(saveImage));
router.get("/getImage", catchErrors(getImage));
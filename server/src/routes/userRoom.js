import express from "express";
import { catchErrors } from "../handlers/errorHandlers.js";
import {createUserRoom, getRoomUsers} from "../controllers/userRoomController.js";
export const router = express.Router()
import {auth} from '../middleware/auth.js'


router.post("/createUserRoom", catchErrors(createUserRoom));
router.get("/getRoomUsers", catchErrors(getRoomUsers));

//module.exports = router

import mongoose from 'mongoose'
import { UserSchema } from '../models/UserSchema.js'
import bcrypt from "bcryptjs";
import { UserRoomSchema } from '../models/UserRoomSchema.js'
const UserRoom = new mongoose.model("UserRoom", UserRoomSchema)



const User = new mongoose.model("User", UserSchema)
const users = [];

// Join User to room

export async function userJoin(user, room) {
    
    const userRoom = new UserRoom({
        user,
        room
    });
    await userRoom.save();
    return userRoom;
}

// Get the current user

export async function getCurrentUser(id) {
    return User.find(user => user._id === id)
}



// Get room users

export async function getRoomUsers(room) {

    const roomUsers = await UserRoom.find({
        room,
      });
  
      const users = roomUsers.map(r=>r.user)
      const Users = await User.find({
          'user': { $in: [users]}
        });
    return Users;
}

//module.exports = {
//    userJoin,
//    getCurrentUser,
//    getRoomUsers,
//    userLeave
//}
import mongoose from 'mongoose'
import { ImageSchema } from '../models/ImageSchema.js'
import jwt from "jwt-then";
import sha256 from 'js-sha256';
import { UserSchema } from '../models/UserSchema.js'

const Image = new mongoose.model("Image", ImageSchema)
const User = new mongoose.model("User", UserSchema)



export const saveImage = async (req, res) => {
    const { room } = req.body;

    const obj = {
        room: req.body.room,
        image: {
            data: fs.readFileSync(path.join(__dirname + '/uploads/' + req.body.image)),
            contentType: 'image/png'
        }
    }  
    const imageExists = await Image.findOne({
      room
    });
  
    if (!imageExists){
        const image = new Image(obj);
        await image.save();
        console.log("image: ", image)
    }
  
    res.json({
      message: "Image saved",
    });
  };


  export const getImage = async (req, res) => {

    const { room } = req.query;
    const image = await Image.find({
      room,
    });
    if (!image) throw "Image does not exist";

    res.json({
      message: "image found",
      image,
    });
  };

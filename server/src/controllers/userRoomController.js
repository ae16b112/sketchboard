import mongoose from 'mongoose'
import { UserRoomSchema } from '../models/UserRoomSchema.js'
import jwt from "jwt-then";
import sha256 from 'js-sha256';
import { UserSchema } from '../models/UserSchema.js'

const UserRoom = new mongoose.model("UserRoom", UserRoomSchema)
const User = new mongoose.model("User", UserSchema)


export const createUserRoom = async (req, res) => {
    const { user, room } = req.body;

      
    const userRoomExists = await UserRoom.findOne({
      user,
      room
    });

  
    //if (userRoomExists) throw "User already exists in this room";
  
    if (!userRoomExists){
        const userRoom = new UserRoom({
            user,
            room
        });
        await userRoom.save();
        console.log("userRoom: ", userRoom)
    }
  
    res.json({
      message: "User [" + user.username + "] added to room [" + room + "]",
    });
  };


export const getRoomUsers = async (req, res) => {

    const { room } = req.query;
    console.log("room: ", room)
    const roomUsers = await UserRoom.find({
      room,
    });
    console.log(roomUsers)

    const user_ids = roomUsers.map(r=>r.user._id.toString())

    let Users =[]
    for (const id of user_ids){
      const user = await User.find({_id:id})
      Users.push(user)
    }
    
    res.json({
      message: "Here are all the users in this room",
      Users,
    });
};

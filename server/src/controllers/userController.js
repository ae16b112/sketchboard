import mongoose from 'mongoose'
import { UserSchema } from '../models/UserSchema.js'
import bcrypt from "bcryptjs";
import jwt from "jwt-then";
import sha256 from 'js-sha256';

const User = new mongoose.model("User", UserSchema)

export const register = async (req, res) => {
    const { username, email, password, firstName, lastName } = req.body;

    if (!username || !email || !password || !firstName || !lastName) throw "All the fields are required";

    const emailRegex = /@gmail.com|@yahoo.com|@hotmail.com|@live.com/;
  
    if (!emailRegex.test(email)) throw "Email is not supported from your domain.";
    if (password.length < 6) throw "Password must be atleast 6 characters long.";
  
    const userExists = await User.findOne({
      email,
    });
  
    if (userExists) throw "User with same email already exits.";
  
    const user = new User({
      username,
      email,
      password: sha256(password + "secret"),
      firstName,
      lastName,
    });
  
    await user.save();
  
    res.json({
      message: "User [" + username + "] registered successfully!",
    });
  };


  export const login = async (req, res) => {
    const { email, password } = req.body;

    if (!email) throw "Email is required";
    if (!password) throw "password is required";


    const user = await User.findOne({
      email,
      password: sha256(password + "secret"),
    });
    console.log("user: ", user)
  
    if (!user) throw "user not found.";
  
    const token = await jwt.sign({ id: user.id }, "secret");
  
    res.json({
      message: "User logged in successfully!",
      token,
      user,
    });
  };

import mongoose from 'mongoose';
const { Schema } = mongoose;

export const UserRoomSchema = new Schema({
  room: {
    type: Number,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "User",
  },
});


import mongoose from 'mongoose';
const { Schema } = mongoose;

export const ImageSchema = new Schema({
    room: Number,
    image:
    {
        data: Buffer,
        contentType: String
    }
});

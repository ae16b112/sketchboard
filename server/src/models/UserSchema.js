import mongoose from 'mongoose';
const { Schema } = mongoose;

// Create Schema
export const UserSchema = new Schema({
  username: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  firstName: {
      type: String,
      required: true
  },
  lastName: {
    type: String,
    required: true
  }, 
});


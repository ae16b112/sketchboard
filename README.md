# SketchBoard

A collaborative sketch board using Nodejs, Reactjs, MongoDB, and socket.io

## Getting started

## How to Run locally

1. Setup the Client
```
cd client 
npm install
```

2. Setup the Server

```
cd server
npm install
```

3. Run Client Locally

```
cd client
npm start
```

4. Run Server Locally

```
cd server
node server.js
```

This runs the client on `localhost:3000` and the server runs on `localhost:5000`.

## Tech Stack used

Client
* React.js
* Web Canvas
* Socket IO Client

Server
* Node.js
* Socket IO Server
* MongoDB

## Tech Stack used

User
* firstName
* lastName
* username
* email
* password

UserRoom
* user (object)
* room

UserRoom
* image 
* room






import React from "react";

const Homepage = (props, {setLoginUser}) => {
  React.useEffect(() => {
    const token = localStorage.getItem("CC_Token");
    console.log(token);
    if (!token) {
      props.history.push("/register");
    } else {
      props.history.push("/playground");
    }
    // eslint-disable-next-line
  }, [0]);
  return ( 
        <div className="homepage">
            <h1>Hello Homepage</h1>
            <div className="button" onClick={() => setLoginUser({})}>Logout</div>
        </div> 
  );
};

export default Homepage;
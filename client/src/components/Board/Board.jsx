import React from 'react'
import {useEffect, useState} from "react"
import "./Board.css"
import randomColor from "randomcolor";
import axios from "axios";
import { useParams } from "react-router-dom";


function Board({socket}) {
    let room
    let timeout;
    let restore_array=[];
    let index = -1;
    let [userData, setUserData] = useState([])
    useEffect(() => 
        socket.on("annotation-data", (data) => {
            let image = new Image();
            var canvas = document.querySelector("#board");
            var ctx = canvas.getContext('2d');
            image.onload = () => {
                ctx.drawImage(image,0,0);
            }
            image.src = data;
        }),[]
    )
    
    useEffect(() => {
        let canvas = document.querySelector("#board");
        let imgin = document.getElementById('imgin')
        let ctx = canvas.getContext('2d')


        imgin.onchange=function() {
            var img = new Image()
            img.onload = function() {
                canvas.width = this.width
                canvas.height = this.height
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                ctx.drawImage(this, 0, 0)
                //URL.revokeObjectURL(this.src)
            }
            img.src = URL.createObjectURL(this.files[0])
        }

    },[])


    useEffect(() => {
        //console.log(props)
        const params = new URLSearchParams(window.location.search) // id=123
        room = params.get('room')
        drawOnCanvas();
    })
    
    const drawOnCanvas = () => {
        let canvas = document.querySelector('#board');
        let ctx = canvas.getContext('2d');

        let sketch = document.querySelector('#sketch');
        let sketch_style = getComputedStyle(sketch);
        canvas.width = parseInt(sketch_style.getPropertyValue('width'));
        canvas.height = parseInt(sketch_style.getPropertyValue('height'));
        canvas.style.width = `${parseInt(sketch_style.getPropertyValue('width'))}px`;
        canvas.style.height = `${parseInt(sketch_style.getPropertyValue('height'))}px`;

        var mouse = {x: 0, y: 0};
        var last_mouse = {x: 0, y: 0};

        /* Mouse Capturing Work */
        canvas.addEventListener('mousemove', function(e) {
            last_mouse.x = mouse.x;
            last_mouse.y = mouse.y;

            mouse.x = e.pageX - this.offsetLeft;
            mouse.y = e.pageY - this.offsetTop;
        }, false);


        /* Drawing on Paint App */
        ctx.lineWidth = 5;
        ctx.lineJoin = 'round';
        ctx.lineCap = 'round';
        ctx.strokeStyle = randomColor();
        ctx.fillStyle = 'white';

        canvas.addEventListener('mousedown', function(e) {
            canvas.addEventListener('mousemove', onPaint, false);
        }, false);

        canvas.addEventListener('mouseup', function() {
            canvas.removeEventListener('mousemove', onPaint, false);
        }, false);

        let onPaint = function() {
            ctx.beginPath();
            ctx.moveTo(last_mouse.x, last_mouse.y);
            ctx.lineTo(mouse.x, mouse.y);
            ctx.closePath();
            restore_array.push(ctx.getImageData(0,0, canvas.width, canvas.height))
            index += 1;
            ctx.stroke();

            if(timeout !== undefined) clearTimeout(timeout);
            timeout = setTimeout(function(){
                var base64ImageData = canvas.toDataURL("image/png");
                socket.emit("annotation-data", base64ImageData);
            }, 1000)
            console.log(index)

        };

    }

    const download = async () => {
        let canvas = document.querySelector('#board');
        const image = canvas.toDataURL('image/png');
        const blob = await (await fetch(image)).blob();
        const blobURL = URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = blobURL;
        link.download = "image.png";
        link.click();
    }

    const save = async () => {
        let canvas = document.querySelector('#board');
        const image = canvas.toDataURL('image/png');
        const blob = await (await fetch(image)).blob();
        const blobURL = URL.createObjectURL(blob);
        const data = {room, blobURL}
        axios.post('http://localhost:5000/image/saveImage', data)
        .then(res => {
           console.log(res.data + 'this is data after api call');
        })
        .catch(err => console.log(err));
    }

    const clearCanvas = () => {
        let canvas = document.querySelector('#board');
        const ctx = canvas.getContext("2d")
        ctx.fillStyle = "white"
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.fillRect(0, 0, canvas.width, canvas.height)
        restore_array = []
        index = -1
    }

    const undo = () =>{
        let canvas = document.querySelector('#board');
        let ctx = canvas.getContext("2d")
        console.log("index", index)

        if (index <= 0){
            console.log(index)
            clearCanvas()
        }else{
            index -= 1;
            //restore_array.pop();
            ctx.putImageData(restore_array[index], 0, 0);
        }
        console.log(restore_array)
    }

    const redo = () =>{
        let canvas = document.querySelector('#board');
        let ctx = canvas.getContext("2d")
        if (index <= 0){
            clearCanvas()
        }else{
            index += 1;
            //restore_array.pop();
            if (restore_array[index]){
                ctx.putImageData(restore_array[index], 0, 0);
            }
        }
    }

    const fetchRoomUsers = async() => {
        const res = await axios.get('http://localhost:5000/room/getRoomUsers',{
            params: {
                room: room
            }}, 
            //config
        );
        setUserData([...res.data.Users])
    }

 

    return (
            <div className="sketch" id="sketch">
                <div className="tools">
                    <button onClick={fetchRoomUsers} type="button" className="button">BoardUsers</button>

                    <input type="file" accept="image/*" id="imgin"/>
                    <button onClick={download}>Download</button>
                    <button onClick={save}>Save</button>
                    <button onClick={undo} type="button" className="button">Undo</button>
                    <button onClick={redo} type="button" className="button">Redo</button>
                    <button onClick={clearCanvas}>Clear</button>



                </div>
                {userData.map((item, index) => (
                        <div>
                            <div>
                            <p className="userlist">{item[0].username}</p>
                            </div>
                        </div>

                    ))}
                <canvas className="board" id="board"></canvas>
            </div>    
    )
}

export default Board










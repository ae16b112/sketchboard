import React from 'react'
import {useState} from "react"
import { useHistory, withRouter } from "react-router-dom";
import "./login.css"
import io from 'socket.io-client';
import makeToast from "../../Toaster";
import axios from "axios";


const socket = io.connect('http://localhost:5000/user', {transports: ['websocket']});


const LoginPage = (props) => {
  
  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const roomRef = React.createRef();
  let history = useHistory();


  const loginUser = async() => {
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    const room = roomRef.current.value;

    const response = await axios.post("http://localhost:5000/user/login", {
        email,
        password,
    })

    const postData = { user:response.data.user, room };
    const roomUser = await axios.post('http://localhost:5000/room/createUserRoom', postData)

    makeToast("success", response.data.message);
    localStorage.setItem("CC_Token", response.data.token);
    socket.emit('joinRoom', { username:response.data.user.username, room })
    // eslint-disable-next-line no-restricted-globals
    history.push(`/playground?username=${response.data.user.username}&room=${room}`);
    props.setupSocket();
  }  

  return (
    <div className="container">
      <div className="login">
        <h1>Login</h1>
        <label htmlFor="email">Email</label>
        <input type="email" required name="email" id="email" placeholder="abc@example.com" ref={emailRef}/>
        <label htmlFor="password">Password</label>
        <input type="password" name="password" id="password" placeholder="Your Password" ref={passwordRef}/>
        <label htmlFor="room">Room</label>
        <input type="number" name="number" id="number" placeholder="Enter Room" ref={roomRef}/>
        <button onClick={loginUser}>Login</button>
      </div>
    </div>
    
  );  
};

export default withRouter(LoginPage);

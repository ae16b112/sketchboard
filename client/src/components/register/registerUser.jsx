
import React from "react";
import axios from "axios";
import makeToast from "../../Toaster";
import { useHistory, withRouter } from "react-router-dom";
import "./register.css"


const RegisterPage = (props) => {
  const usernameRef = React.createRef();
  const emailRef = React.createRef();
  const passwordRef = React.createRef();
  const firstNameRef = React.createRef();
  const lastNameRef = React.createRef();
  const history = useHistory()

  const registerUser = (props) => {
    console.log("usernameRef.current.value: ",usernameRef.current.value )
    const username = usernameRef.current.value;
    const email = emailRef.current.value;
    const password = passwordRef.current.value;
    const firstName = firstNameRef.current.value;
    const lastName = lastNameRef.current.value;




    axios.post("http://localhost:5000/user/register", {
        username,
        email,
        password,
        firstName,
        lastName
    })
      .then((response) => {
        makeToast("success", response.data.message);
        history.push("/login");
      })
      .catch((err) => {
        // console.log(err);
        if (
          err &&
          err.response &&
          err.response.data &&
          err.response.data.message
        )
          makeToast("error", err.response.data.message);
      });
  };

  return (
    <div className="container">
      <div className="register">
      <h1>Registration</h1>
          <label htmlFor="username">Username</label>
          <input
            type="text"
            name="username"
            id="username"
            placeholder="John Doe"
            ref={usernameRef}
          />
          <label htmlFor="firstName">First Name</label>
          <input
            type="text"
            name="firstName"
            id="firstName"
            placeholder="John"
            ref={firstNameRef}
          />
          <label htmlFor="lastName">Last Name</label>
          <input
            type="text"
            name="lastName"
            id="lastName"
            placeholder="Doe"
            ref={lastNameRef}
          />
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            id="email"
            placeholder="abc@example.com"
            ref={emailRef}
          />
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            id="password"
            placeholder="Your Password"
            ref={passwordRef}
          />
        <button onClick={registerUser}>Register</button>
        <div>or</div>
        <button onClick={() => history.push("/login")}>Login</button>
      </div>
    </div>
  );
};

export default RegisterPage;
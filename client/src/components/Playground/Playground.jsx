import React, {useEffect} from 'react'
import "./Playground.css"
import Board from "./../Board/Board"
import io from 'socket.io-client';
import {useLocation} from "react-router-dom"

let monsterPath = process.env.PUBLIC_URL + '/monsters';

function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

function Playground() {
    const socket = io.connect('http://localhost:5000/', {transports: ['websocket']});
    let query = useQuery();
    const username=query.get("username");
    const room = query.get("room");
    console.log(username)
    useEffect(() => {
        socket.emit('joinRoom', { username, room });
        socket.on('roomUsers', ({ room, users }) => {
        console.log(room, users);
        });
        socket.on('message', (message) => {
            console.log(message);
        })
    })
    return (
        <div className="container">
            <header className="container-header">
                <img src={`${monsterPath}/${room}.png`} className="App-logo" alt="logo" />
                <div className="board-container">
                    <Board socket={socket}></Board>
                </div>
            </header>
        </div>
    )
}

export default Playground

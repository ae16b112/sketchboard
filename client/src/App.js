//import './App.css';
//import Playground from "./components/Playground/Playground"
//import LoginPage from "./components/Join/loginUser";
//import {
//  BrowserRouter as Router,
//  Switch,
//  Route,
//  Link
//} from "react-router-dom";

import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LoginPage from "./components/login/loginUser";
import RegisterPage from "./components/register/registerUser";
import Playground from "./components/Playground/Playground"
import Homepage from "./components/homepage/homepage";
import io from "socket.io-client";
import makeToast from "./Toaster";


function App() {
  const [socket, setSocket] = React.useState(null);
  const [ user, setLoginUser] = React.useState({})


  const setupSocket = () => {
    const token = localStorage.getItem("CC_Token");
    if (token && !socket) {
      const newSocket = io("http://localhost:5000", {
        query: {
          token: localStorage.getItem("CC_Token"),
        },
      });

      newSocket.on("disconnect", () => {
        setSocket(null);
        setTimeout(setupSocket, 3000);
        makeToast("error", "Socket Disconnected!");
      });

      newSocket.on("connect", () => {
        console.log("socket connected")
        makeToast("success", "Socket Connected....!");
      });
      setSocket(newSocket);
      //return newSocket
    }
  };

  React.useEffect(() => {
    setupSocket();
    //eslint-disable-next-line
  }, [setSocket]);

  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
        <Route path="/" component={Homepage} exact />
          <Route
            path="/login"
            render={() => <LoginPage setupSocket={setupSocket} />}
            exact
          />
          <Route path="/register" component={RegisterPage} exact />
          <Route path="/playground">
          <Playground />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}


//function App() {
//  return (
//    <Router>
//      <Switch>
//        <Route exact path="/">
//          <LoginPage/>
//        </Route>
//        <Route path="/playground">
//          <Playground />
//        </Route>
//      </Switch>
//    </Router>
//  );
//}

export default App;



//const setupSocket = () => {
//  const token = localStorage.getItem("CC_Token");
//  if (token && !socket) {
//    const newSocket = io("http://localhost:5000", {
//      query: {
//        token: localStorage.getItem("CC_Token"),
//      },
//    });
//
//    newSocket.on("disconnect", () => {
//      setSocket(null);
//      setTimeout(setupSocket, 3000);
//      makeToast("error", "Socket Disconnected!");
//    });
//
//    newSocket.on("connect", () => {
//      console.log("socket connected")
//      makeToast("success", "Socket Connected....!");
//    });
//
//    setSocket(newSocket);
//  }
//};
//
//React.useEffect(() => {
//  setupSocket();
//  //eslint-disable-next-line
//}, []);